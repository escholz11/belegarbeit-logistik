\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Aufgabenstellung und Motivation}{2}{section.2}
\contentsline {section}{\numberline {3}Ideenfindung}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Erste \IeC {\"U}berlegungen}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Konkretere Ausf\IeC {\"u}hrung}{5}{subsection.3.2}
\contentsline {section}{\numberline {4}Konkurrenzanalyse}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Wie arbeitet die Konkurrenz?}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Welche technischen Innovationen existieren am Lieferservicemarkt?}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Schlussfolgerung f\IeC {\"u}r die Pizza Box}{13}{subsection.4.3}
\contentsline {section}{\numberline {5}Darstellung eines Anwendungsszenarios}{14}{section.5}
\contentsline {section}{\numberline {6}Anforderungsanalyse}{15}{section.6}
\contentsline {subsection}{\numberline {6.1}Anforderungen an das Flottentelematik-System}{15}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Anforderungen an die Kommunikation des Flottentelematik-Systems}{17}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Anforderungen an den Bestellvorgang}{19}{subsection.6.3}
\newpage 
\contentsline {section}{\numberline {7}Marktanalyse und Entscheidungsfindung}{21}{section.7}
\contentsline {subsection}{\numberline {7.1}Vergleich der Marktteilnehmer}{21}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Kosten-Nutzen-Analyse}{22}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Ausarbeitung eines Bewertungsschemas}{23}{subsection.7.3}
\contentsline {section}{\numberline {8}Innovation durch telematische Systeme}{25}{section.8}
\contentsline {section}{\numberline {9}Wirtschaftlichkeitsanalyse}{29}{section.9}
\contentsline {section}{\numberline {10}Fazit}{33}{section.10}
\contentsline {section}{Quellenverzeichnis}{34}{section*.3}
\contentsline {section}{Abbildungsverzeichnis}{36}{section*.3}
\contentsline {section}{Tabellenverzeichnis}{37}{section*.4}
